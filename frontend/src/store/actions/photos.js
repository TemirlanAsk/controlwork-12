import axios from '../../axios-api';
import {push} from "react-router-redux";
import {
  CREATE_PHOTO_SUCCESS, DELETE_PHOTO_SUCCESS, FETCH_PHOTOS_SUCCESS,

} from "./actionTypes";
import {NotificationManager} from "react-notifications";

export const fetchPhotosSuccess = photos => {
  return {type: FETCH_PHOTOS_SUCCESS, photos};
};


export const deletePhotoSuccess = photo => {
  return {type: DELETE_PHOTO_SUCCESS, photo};
};

export const fetchPhotos = () => {
  return (dispatch, getState) => {
    const user = getState().users.user;
    const isAdmin = user && user.role === 'admin';
    axios.get(isAdmin ? '/photos/admin' : '/photos').then(
      response => dispatch(fetchPhotosSuccess(response.data))
    );
  }
};

export const createPhotoSuccess = () => {
  return {type: CREATE_PHOTO_SUCCESS};
};


export const createPhoto = photoData => {
  return (dispatch, getState) => {
    const headers = {'Token': getState().users.token};
    return axios.post('/photos', photoData, {headers}).then(
      response => {
        dispatch(createPhotoSuccess());
        dispatch(push('/'));
        NotificationManager.success('Success', 'New photo created successfully and under moderator approval');
      },
      error => {
        NotificationManager.error('Error', 'Could not create new photo');
      }
    );
  };
};

export const photoDelete = (id) => {
  return (dispatch, getState) => {
    axios.delete('/photos/' + id, {headers: {'Token': getState().users.token}}).then(
      response => {
        dispatch(deletePhotoSuccess());
        dispatch(push('/'));
        NotificationManager.success('Success', 'Deleted successfully');
      },
      error => {
        NotificationManager.error('Error', 'Could not delete photo');
      }
    );
  };
};

