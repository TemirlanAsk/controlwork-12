const path = require('path');

const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, '/public/uploads'),
  db: {
    url: 'mongodb://localhost:27017',
    name: 'photos'
  },
  jwt: {
    secret: 'some kinda very secret string',
    expires: '7d'
  },
  facebook: {
    appId: "416546525526906", // Enter your app ID here
    appSecret: "06c00b14f83929f0bc9bd33748e8e110" // Enter your app secret here
  }
};

