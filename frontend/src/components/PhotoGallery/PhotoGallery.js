import React from 'react';
import {Col, Grid, Image, Modal, Panel, Row, Thumbnail} from "react-bootstrap";
import {NavLink} from "react-router-dom";
import PropTypes from 'prop-types';

import config from '../../config';

import notFound from '../../assets/images/not-found.png';

const PhotoGallery = props => {
    let image = notFound;

    if (props.image) {
        image = config.apiUrl + '/uploads/' + props.image;
    }
    console.log(props.username);
    return (

        <NavLink to={'/photos/' + props.id}>

            <Col xs={6} md={3}>
                 <span> By:
                     {props.username}
                    </span>
                <div onClick={props.showModal}>
                <Thumbnail href="#" alt="171x180" src={image}/>
                </div>
                <b> Name:
                    {props.title}
                </b>

            </Col>
        </NavLink>
    );
};

PhotoGallery.propTypes = {
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    username: PropTypes.string.isRequired,
    image: PropTypes.string

};

export default PhotoGallery
;