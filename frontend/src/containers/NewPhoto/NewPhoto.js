import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";

import PhotoForm from "../../components/PhotoForm/PhotoForm";
import {createPhoto} from "../../store/actions/photos";

class NewPhoto extends Component {
    createPhoto = photoData => {
        for(let key of photoData.entries()) {
            console.log(key)
        }
        photoData.append('user', this.props.user._id);
        this.props.onPhotoCreated(photoData, this.props.user.token).then(() => {
            this.props.history.push('/');
        });
    };

    render() {
    return (
      <Fragment>
        <PageHeader>Add new Photo</PageHeader>
        <PhotoForm
          onSubmit={this.createPhoto}
        />
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
    return {
        user: state.users.user
    }
};

const mapDispatchToProps = dispatch => ({
    onPhotoCreated: photoData => {
    return dispatch(createPhoto(photoData))
  }
});


export default connect(mapStateToProps, mapDispatchToProps)(NewPhoto);