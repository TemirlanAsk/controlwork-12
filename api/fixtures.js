const mongoose = require('mongoose');
const config = require('./config');

const Photo = require('./models/Photo');
const User = require('./models/User');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
  try {
    await db.dropCollection('photos');
    await db.dropCollection('users');
  } catch (e) {
    console.log('Collections were not present, skipping drop...');
  }

  const [userUsername, adminUsername] = await User.create({
    username: 'user',
    password: '123',
    role: 'user'
  }, {
    username: 'admin',
    password: 'admin123',
    role: 'admin'
  });


  await Photo.create({
    title: 'Ala-archa',
    user: userUsername._id,
    image: 'ala-archa.jpg',

  }, {
    title: 'horses',
    user: userUsername._id,
    image: 'horses',
  }, {
      title: 'mountain',
      user: userUsername._id,
      image: 'mountain',
  });


  db.close();
});