import React from 'react';
import {Route, Switch} from "react-router-dom";

import NewPhoto from "./containers/NewPhoto/NewPhoto";
import Register from "./containers/Register/Register";
import Photos from "./containers/Photos/Photos";
import Login from "./containers/Login/Login";


const Routes = () => (
  <Switch>
        <Route path="/" exact component={Photos}/>
        <Route path="/register" exact component={Register}/>
        <Route path="/login" exact component={Login}/>
        <Route path="/photos/new" exact component={NewPhoto}/>
  </Switch>
);

export default Routes;