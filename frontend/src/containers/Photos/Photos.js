import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Button, PageHeader, Row, Grid, Col, Image} from "react-bootstrap";
import {fetchPhotos} from "../../store/actions/photos";
import {Link} from "react-router-dom";

import PhotoGallery from '../../components/PhotoGallery/PhotoGallery';
import Modal from "react-bootstrap/es/Modal";
import config from "../../config";

class Photos extends Component {
    state = {
        show: false,
        photo: ''
    };
  componentDidMount() {
    this.props.onFetchPhotos();
  }
    modalClose = () => {
        this.setState({ show: false });
    };

    modalShow = (img) => {
        this.setState({ show: true, photo: img});
    };

  render() {

      let image = config.apiUrl + '/uploads/' + this.state.photo;

    return (
      <Fragment>
        <PageHeader>
            Photo Gallery
          { this.props.user && this.props.user.role === 'admin' &&
            <Link to="/photos/new">
              <Button bsStyle="primary" className="pull-right">
                Add photo
              </Button>
            </Link>
          }
        </PageHeader>
          <Grid>
              <Row>
                {this.props.photos.map(photo => (
                  <PhotoGallery
                    key={photo._id}
                    id={photo._id}
                    title={photo.title}
                    image={photo.image}
                    username={photo.user.username}
                    showModal={() => this.modalShow(photo.image)}
                  />
                ))}
                  <div>
                      <Modal bsSize="large" show={this.state.show} onHide={this.modalClose}>
                          <Modal.Header closeButton>
                          </Modal.Header>
                          <Col xs={12} md={8}>
                              <Image
                                  responsive
                                  src={image}
                              />
                          </Col>
                          <Modal.Footer>
                              <Button bsStyle="success" type="submit" onClick={this.modalClose}>Close</Button>
                          </Modal.Footer>
                      </Modal>
                  </div>
              </Row>
          </Grid>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    photos: state.photos.photos,
    user: state.users.user
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchPhotos: () => dispatch(fetchPhotos())
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Photos);