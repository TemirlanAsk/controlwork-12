const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const Photo = require('../models/Photo');

const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const config = require('../config');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

const createRouter = () => {
  // Photo index
    router.get('/', (req, res) => {
        Photo.find().populate('user')
            .then(results => res.send(results))
            .catch(() => res.sendStatus(500));
    });

  // Photo create
  router.post('/', [auth, upload.single('image')], (req, res) => {
    const photoData = req.body;

    if (req.file) {
      photoData.image = req.file.filename;
    } else {
      photoData.image = null;
    }

    photoData.user = req.user._id;

    const photo = new Photo(photoData);

    photo.save()
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error));
  });

  router.get('/admin', auth, permit('admin'), (req, res) => {
      Photo.find().populate('user')
      .then(results => res.send(results))
      .catch(() => res.sendStatus(500));
  });

  // Photo get by ID
  router.get('/:id', (req, res) => {
      Photo.findOne({_id: req.params.id}).populate('user')
      .then(results => res.send(results))
      .catch(() => res.sendStatus(500));
  });


  router.put('/:id', [auth, permit('admin')], async (req, res) => {
    console.log(req.params.id);
    console.log('here');
    const photo = await Photo.findOne({_id: req.params.id});
      photo.published = true;
      photo.save()
      .then(photo => res.send(photo))
      .catch(error => res.status(400).send(error));
  });

  router.put('/:id/rate', auth, async (req, res) => {
  console.log(req.body.rate);

    const photo = await Photo.findOneAndUpdate({_id: req.params.id}, {$push: {rate: req.body.rate}});
      photo.published = true;
  });

  router.delete('/:id', [auth, permit('admin')], async (req, res) => {
    const photo = await Photo.findOneAndRemove({_id: req.params.id});
    res.send(photo)
  });

  return router;
};

module.exports = createRouter;